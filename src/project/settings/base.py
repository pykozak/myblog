import json
import os

from django.core.exceptions import ImproperlyConfigured

SRC_DIR = os.path.dirname(os.path.dirname(os.path.dirname(__file__)))
ROOT_DIR = os.path.dirname(SRC_DIR)

with open('secrets.json') as f:
    secrets = json.load(f)


def get_secret(setting, secrets=secrets):
    try:
        return secrets[setting]
    except KeyError:
        raise ImproperlyConfigured(
            'Set the {0} environment variable'.format(setting)
        )

SECRET_KEY = get_secret('SECRET_KEY')
IMGUR_CLIENT_ID = get_secret('IMGUR_CLIENT_ID')
IMGUR_API_KEY = get_secret('IMGUR_API_KEY')
RSS_TITLE = get_secret('RSS_TITLE')
RSS_DESCRIPTION = get_secret('RSS_DESCRIPTION')
RSS_LINK = get_secret('RSS_LINK')
