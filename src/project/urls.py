from django.conf.urls import patterns, include, url

from django.contrib import admin
admin.autodiscover()

from django.conf.urls.static import static
from django.conf import settings


urlpatterns = patterns('',
    url(r'^admin/', include(admin.site.urls)),
    url(r'^', include('entries.urls')),
    url(r'^tagi/', include('tags.urls')),
    url(r'^kontakt/', include('contact.urls')),
    url(r'^rss/', include('rss.urls')),
    url(r'^archiwum/', include('archive.urls')),
) + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
