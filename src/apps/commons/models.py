from django.db import models
from django.core.exceptions import ImproperlyConfigured


class PGCharField(models.CharField):
    """Odpowiednik CharField zoptymalizowany pod bazę PostgreSQL."""

    def get_internal_type(self):
        return 'TextField'


class PGEmailField(models.EmailField):
    """Odpowiednik EmailField zoptymalizowany pod bazę PostgreSQL."""

    def get_internal_type(self):
        return 'TextField'


class BaseModel(models.Model):
    """Bazowy model admina przeznaczony do dziedzicznia przez inne klasy."""
    created_at = models.DateTimeField(
        verbose_name='Data utworzenia',
        auto_now_add=True
    )

    class Meta:
        abstract = True

    def delete(self, using=None):
        """Usuwa instancję modelu z bazy.

        Nie zaleca się usuwać instancji modelu z bazy. W wiekszości przypadków
        jest to błąd. Dlatego lepszym wyjściem jest oflagowanie instancji
        statusem 'usunięty'.

        Raises:
            ImproperlyConfigured
        """
        raise ImproperlyConfigured(
            'Przeciąż metodę klasy jeśli chcesz usunąć instancję modelu.')