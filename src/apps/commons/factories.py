import itertools


def create_text_gen(template):
    """Tworzy prosty generator to budowania tekstu z rosnącą liczbą.

    Funkcja jest bardzo użyteczna przy tworzeniu własnych fabryk, które
    wraz z tworzeniem kolejnej instancji muszą inkrementować licznik dla
    kolejnej nazwy.

    Przykład:
        >>> username_gen = create_text_gen('username_{0}')
        >>> next(username_gen)
        username_1
        >>> next(username_gen)
        username_2

    Args:
        template (str): szablon do generowania tekstu
    Returns:
        Generator.
    Raises:
        ValueError.
    """
    if '{0}' not in template:
        raise ValueError('template musi zawierać w sobie: {0}')
    return (template.format(x) for x in itertools.count(start=1))