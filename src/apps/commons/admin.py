from django.contrib import admin
from django.core.urlresolvers import RegexURLPattern


class _PermissionsMixin:
    """Mixin pozwalający za pomocą krotki nadać wybrane uprawnienia."""
    permissions = ()

    def has_add_permission(self, *args, **kwargs):
        return 'add' in self.permissions

    def has_change_permission(self, request, obj=None):
        return 'change' in self.permissions

    def has_delete_permission(self, *args, **kwargs):
        return 'delete' in self.permissions


class _UrlPatternsMixin:
    """Mixin pozwalający dodać urlpatterns w sposób deklaracyjny."""
    urlpatterns = ()

    def get_urls(self):
        base_urls = super(_UrlPatternsMixin, self).get_urls()
        return self._get_extra_urls() + base_urls

    def _get_extra_urls(self):
        return [RegexURLPattern(
            regex=p._regex,
            callback=self.admin_site.admin_view(p.callback),
            default_args=p.default_args,
            name=p.name
        ) for p in self.urlpatterns]


class BaseModelAdmin(_PermissionsMixin, _UrlPatternsMixin, admin.ModelAdmin):
    """Bazowy model admina przeznaczony do dziedzicznia przez inne klasy.

    W odróżnieniu od ```ModelAdmin``` programista musi jawnie znaznaczyć
    co ma być dostępne.
    """
    actions = None