from django.conf.urls import patterns, url

from . import feeds

urlpatterns = patterns('',
    url(
        regex=r'^$',
        view=feeds.LatestEntriesFeed(),
        name='feeds#latest_entries'
    ),
)
