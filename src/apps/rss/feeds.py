from django.contrib.syndication.views import Feed
from django.core.urlresolvers import reverse
from django.conf import settings

from entries.models import Entry


class LatestEntriesFeed(Feed):
    title = settings.RSS_TITLE
    link = settings.RSS_LINK
    description = settings.RSS_DESCRIPTION
    limit = settings.RSS_ENTRY_LIMIT

    def items(self):
        return Entry.objects.get_latest_published_entries(self.limit)

    def item_description(self, item):
        return item.render()

    def item_title(self, item):
        return item.subject

    def item_link(self, item):
        return reverse('entries#entry_detail', args=[item.id])