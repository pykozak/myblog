from django.contrib import admin
from django.core.urlresolvers import reverse
from django.utils.html import format_html

from commons.admin import BaseModelAdmin

from . import models


class CommentEntryInline(admin.StackedInline):
    model = models.CommentEntry
    fields = ['username', 'email', 'template']
    readonly_fields = fields
    extra = 0
    can_delete = False


class CommentEntryAdmin(BaseModelAdmin):
    permissions = ['change']
    list_display = ['formatted_template', 'username', 'formatted_entry',
                    'created_at', 'status']
    template_max_length = 100

    def formatted_template(self, comment_entry):
        return comment_entry.template[:self.template_max_length]

    formatted_template.short_description = 'Szablon'

    def formatted_entry(self, comment_entry):
        entry = comment_entry.entry
        url = reverse('entries#entry_detail', args=[entry.id])
        return format_html('<a href="{0}">{1}</a>', url, entry.subject)

    formatted_entry.short_description = 'Wpis'


admin.site.register(models.CommentEntry, CommentEntryAdmin)