from django import forms

from users.models import User

from . import models


class CommentEntryForm(forms.ModelForm):

    class Meta:
        model = models.CommentEntry
        fields = ('entry', 'username', 'email', 'template')
        widgets = {'entry': forms.HiddenInput()}

    def __init__(self, request, *args, **kwargs):
        super(CommentEntryForm, self).__init__(*args, **kwargs)
        self.request = request

    def clean_username(self):
        return self.cleaned_data['username'].strip()

    def clean_email(self):
        return self.cleaned_data['email'].strip().lower()

    def clean_template(self):
        template = self.cleaned_data['template'].strip()
        if not template:
            raise forms.ValidationError('To pole jest wymagane.')
        return template

    def save(self, *args, **kwargs):
        if self.request.user.is_authenticated:
            self.instance.created_by = User(id=self.request.user.id)
        return super(CommentEntryForm, self).save(*args, **kwargs)
