from commons.factories import create_text_gen

from . import models


class CommentEntryTestFactory:
    username_gen = create_text_gen('username_{0}')
    email_gen = create_text_gen('username_{0}@email.local')
    template_gen = create_text_gen('template_{0}')

    def create(self, entry, username=None, email=None, template=None):
        return models.CommentEntry.objects.create(
            entry=entry,
            username=username or next(self.username_gen),
            email=email or next(self.email_gen),
            template=template or next(self.template_gen)
        )