from django.conf import settings


def comment_count_name(comment_count, language='pl'):
    if language != settings.LANGUAGE_CODE:
        raise ValueError('Unsupported language')
    return _comment_count_name_pl(comment_count)


def _comment_count_name_pl(comment_count):
    if comment_count == 0:
        return 'Brak komentarzy'
    elif comment_count == 1:
        return '1 komentarz'
    elif comment_count == 2:
        return '2 komentarze'
    elif comment_count in (3, 4):
        return '{0} komentarze'.format(comment_count)
    else:
        return '{0} komentarzy'.format(comment_count)