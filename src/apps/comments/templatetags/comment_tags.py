from django.template import Library

from .. import helpers

register = Library()


@register.filter
def comment_count_name(comment_count):
    return helpers.comment_count_name(comment_count)
