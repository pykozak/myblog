from django.db import models
from django.conf import settings

import markdown

from commons.models import BaseModel


class CommentEntry(BaseModel):
    STATUS_PUBLISHED = 10
    STATUS_HIDDEN = 50
    STATUS_DELETED = 100
    STATUS_CHOICES = (
        (STATUS_PUBLISHED, 'Opublikowany'),
        (STATUS_HIDDEN, 'Ukryty'),
        (STATUS_DELETED, 'Usunięty')
    )
    created_by = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        verbose_name='Utworzone przez',
        null=True
    )
    entry = models.ForeignKey(
        'entries.Entry',
        verbose_name='Wpis'
    )
    username = models.CharField(
        verbose_name='Nick',
        max_length=15
    )
    email = models.EmailField(
        verbose_name='Email',
        blank=True
    )
    template = models.TextField(
        verbose_name='Treść',
        max_length=1000,
        blank=False
    )
    status = models.IntegerField(
        verbose_name='Status',
        choices=STATUS_CHOICES,
        default=STATUS_PUBLISHED
    )

    class Meta:
        verbose_name = 'Komentarz'
        verbose_name_plural = 'Komentarze'

    def render(self):
        return markdown.markdown(self.template, safe_mode=True)

    def is_authenticated(self):
        return self.created_by is not None