from django.contrib import admin

import markdown

from commons.admin import BaseModelAdmin

from . import models


class ContactMessageAdmin(BaseModelAdmin):
    permissions = ['change', 'delete']
    list_display = ['formatted_template', 'username', 'email', 'created_at']
    readonly_fields = ['created_at', 'username', 'email', 'template', 'content']
    template_max_length = 100
    fieldsets = (
        (None, {
            'fields': ('username', 'email', 'created_at', 'content')
        }),
        ('Szczegóły', {
            'fields': ('template',),
            'classes': ('collapse',),
        })
    )

    def formatted_template(self, contact_message):
        return contact_message.template[:self.template_max_length]

    formatted_template.short_description = 'Szablon'

    def content(self, contact_message):
        return markdown.markdown(contact_message.template, safe_mode=True)

    content.short_description = 'Treść'
    content.allow_tags = True

    def get_queryset(self, request):
        queryset = super(ContactMessageAdmin, self).get_queryset(request)
        return queryset.order_by('-created_at')

admin.site.register(models.ContactMessage, ContactMessageAdmin)
