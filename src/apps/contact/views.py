from django.views import generic
from django.shortcuts import render

from . import forms


class ContactMessageFormView(generic.CreateView):
    form_class = forms.ContactMessageForm
    template_name = 'contact/contact.html'

    def form_valid(self, form):
        form.save()
        return render(self.request, 'contact/thank_you.html')