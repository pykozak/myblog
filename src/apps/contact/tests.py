from django.test import TestCase
from django.core.urlresolvers import reverse

from .models import ContactMessage


class ContactMessageFormViewTests(TestCase):

    @classmethod
    def setUpClass(cls):
        cls.url = reverse('contact#contact')

    def get_valid_data(self, **kwargs):
        defaults = {
            'username': 'PythonUser',
            'email': 'pythonuser@email.local',
            'template': 'Hello :)'
        }
        defaults.update(kwargs)
        return defaults

    def test_200_for_method_get(self):
        response = self.client.get(self.url)
        self.assertEqual(200, response.status_code)

    def test_200_for_method_post(self):
        response = self.client.post(self.url, data=self.get_valid_data())
        self.assertEqual(200, response.status_code)

    def test_contact_message(self):
        data = self.get_valid_data()
        self.client.post(self.url, data=data)
        contact_message = ContactMessage.objects.get()
        self.assertEqual(data['username'], contact_message.username)
        self.assertEqual(data['email'], contact_message.email)
        self.assertEqual(data['template'], contact_message.template)

    def test_invalid_data(self):
        data = self.get_valid_data(username='')
        self.client.post(self.url, data=data)
        self.assertEqual(0, ContactMessage.objects.count())
