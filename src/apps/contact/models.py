from django.db import models

from commons.models import BaseModel


class ContactMessage(BaseModel):
    STATUS_NEW = 10
    STATUS_DELETED = 100
    STATUS_CHOICES = (
        (STATUS_NEW, 'Opublikowany'),
        (STATUS_DELETED, 'Usunięty')
    )
    username = models.CharField(
        verbose_name='Nick',
        max_length=15
    )
    email = models.EmailField(
        verbose_name='Email',
    )
    template = models.TextField(
        verbose_name='Treść',
        max_length=1000,
        blank=False
    )
    status = models.IntegerField(
        verbose_name='Status',
        choices=STATUS_CHOICES,
        default=STATUS_NEW
    )

    class Meta:
        verbose_name = 'Wiadomość'
        verbose_name_plural = 'Wiadomości'
