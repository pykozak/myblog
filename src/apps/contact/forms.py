from django import forms

from . import models


class ContactMessageForm(forms.ModelForm):

    class Meta:
        model = models.ContactMessage
        fields = ('username', 'email', 'template')

    def __init__(self, *args, **kwargs):
        super(ContactMessageForm, self).__init__(*args, **kwargs)
        for field in self.fields.values():
            field.label = ''

        self.fields['username'].widget = forms.TextInput(
            attrs={'placeholder': 'Nazwa użytkownika'})
        self.fields['email'].widget = forms.EmailInput(
            attrs={'placeholder': 'Email'})
        self.fields['username'].widget = forms.TextInput(
            attrs={'placeholder': 'Nazwa użytkownika'})
        self.fields['template'].widget = forms.Textarea(
            attrs={'placeholder': 'Wiadomość', 'cols':'10', 'rows': '10'})


    def clean_username(self):
        return self.cleaned_data['username'].strip()

    def clean_email(self):
        return self.cleaned_data['email'].strip().lower()

    def clean_template(self):
        template = self.cleaned_data['template'].strip()
        if not template:
            raise forms.ValidationError('To pole jest wymagane.')
        return template