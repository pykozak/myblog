from django.conf.urls import patterns, url

from . import views

urlpatterns = patterns('',
    url(
        regex=r'^$',
        view=views.ArchiveView.as_view(),
        name='archive#archive'
    ),
)
