from django.views import generic
from django.shortcuts import render

from entries.models import Entry

from . import helpers


class ArchiveView(generic.View):

    def get(self, request):
        entries = Entry.objects.published().order_by('-published_at')
        return render(request, 'archive/archive.html', {
            'groups': helpers.group_entries_by_year_month(entries)})