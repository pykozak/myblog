import itertools

MONTHS = (
    'Styczeń',
    'Luty',
    'Marzec',
    'Kwiecień',
    'Maj',
    'Czerwiec',
    'Lipiec',
    'Sierpień',
    'Wrzesień',
    'Październik',
    'Listopad',
    'Grudzień'
)


def group_entries_by_year_month(entries):
    group_iter = itertools.groupby(entries, key=_get_year_month_text)
    # Returns list because DTL works with generators not well.
    return [(text, list(values)) for text, values in group_iter]


def _get_year_month_text(entry):
    return '{0} {1}'.format(
        entry.published_at.year,
        MONTHS[entry.published_at.month - 1])