from django.test import TestCase
from django.core.urlresolvers import reverse

from entries.factories import EntryTestFactory


class ArchiveViewTests(TestCase):

    @classmethod
    def setUpClass(cls):
        cls.url = reverse('archive#archive')
        cls.entry_factory = EntryTestFactory()

    def test_status_200_fo_method_get(self):
        response = self.client.get(self.url)
        self.assertEqual(200, response.status_code)

    def test_contains_subject(self):
        entry = self.entry_factory.create_published()
        response = self.client.get(self.url)
        self.assertContains(response, entry.subject)