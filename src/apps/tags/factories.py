from commons.factories import create_text_gen

from . import models


class TagTestFactory:
    name_gen = create_text_gen('tag_{0}')

    def create(self, name=None):
        return models.Tag.objects.create(name=name or next(self.name_gen))