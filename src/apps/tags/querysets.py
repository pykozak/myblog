from django.db.models.query import QuerySet

from entries.models import Entry


class TagQuerySet(QuerySet):

    def for_entry(self, entry):
        return self.filter(entrytag__entry=entry)

    def order(self):
        return self.order_by('slug')


class EntryTagQuerySet(QuerySet):

    def published(self):
        return self.filter(entry__status=Entry.STATUS_PUBLISHED)

    def with_slug(self, slug):
        return self.filter(tag__slug=slug)

    def order(self):
        return self.order_by('-entry__published_at')
