from django import forms

from . import models


class TagAdminForm(forms.ModelForm):

    def clean_name(self):
        name = self.cleaned_data['name']
        slug = models.Tag.make_unique_name(name)

        if self.instance and self.instance.slug == slug:
            required_count = 1
        else:
            required_count = 0

        if required_count == models.Tag.objects.filter(slug=slug).count():
            return name
        raise forms.ValidationError('Nazwa jest już zajęta.')
