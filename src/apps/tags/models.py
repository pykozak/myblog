from django.db import models

from slugify import slugify

from commons.models import BaseModel

from . import managers


class Tag(BaseModel):
    name = models.CharField(
        verbose_name='Nazwa',
        max_length=50
    )
    slug = models.SlugField(
        verbose_name='Slug',
        max_length=50,
        unique=True
    )
    description = models.CharField(
        verbose_name='Opis',
        max_length=100,
        blank=True
    )
    objects = managers.TagManager()

    class Meta:
        verbose_name = 'Tag'
        verbose_name_plural = 'Tagi'

    @classmethod
    def make_unique_name(cls, name):
        return slugify(name.lower())

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        self.slug = self.make_unique_name(self.name)
        return super(Tag, self).save()


class EntryTag(BaseModel):
    entry = models.ForeignKey(
        'entries.Entry',
        verbose_name='Wpis'
    )
    tag = models.ForeignKey(
        'Tag',
        verbose_name='Tag'
    )
    objects = managers.EntryTagManager()

    class Meta:
        verbose_name = 'Otagowany wpis'
        verbose_name_plural = 'Otagowane wpisy'

    def __str__(self):
        return self.tag.name