from django.views import generic
from django.shortcuts import render, get_object_or_404

from . import models


class TagListView(generic.View):

    def get(self, request):
        tags = models.EntryTag.objects.get_published_tags()
        return render(request, 'tags/tag_list.html', {'tags': tags})


class TaggedEntryListView(generic.View):

    def get(self, request, tag_slug):
        tag = get_object_or_404(models.Tag, slug=tag_slug)
        entries = models.EntryTag.objects.get_entries_by_tag_slug(tag_slug)
        context = {'tag': tag, 'entries': entries}
        return render(request, 'tags/tagged_entry_list.html', context)

