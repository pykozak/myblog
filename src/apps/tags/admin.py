from django.contrib import admin

from commons.admin import BaseModelAdmin

from . import models
from . import forms


class TagAdmin(BaseModelAdmin):
    permissions = ['add', 'change']
    list_display = ['name', 'created_at', 'has_description']
    form = forms.TagAdminForm

    def get_fieldsets(self, request, tag=None):
        fieldset_creation = ('name', 'description')
        fieldset_edition = ('name', 'slug', 'description')
        return (
            ('Pola do tworzenia/edycji', {
                'fields': fieldset_edition if tag else fieldset_creation
            }),
        )

    def has_description(self, tag):
        return bool(tag.description)

    has_description.short_description = 'Opis?'
    has_description.boolean = True

admin.site.register(models.Tag, TagAdmin)


class EntryTagInline(admin.StackedInline):
    model = models.EntryTag
    extra = 1
