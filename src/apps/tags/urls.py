from django.conf.urls import patterns, url

from . import views

urlpatterns = patterns('',
    url(
        regex=r'^$',
        view=views.TagListView.as_view(),
        name='tags#tag_list'
    ),
    url(
        regex=r'^(?P<tag_slug>[\w-]+)/$',
        view=views.TaggedEntryListView.as_view(),
        name='tags#tagged_entry_list'
    ),

)
