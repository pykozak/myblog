from django.db import models

from . import querysets


class TagManager(models.Manager):

    def get_queryset(self):
        return querysets.TagQuerySet(self.model, using=self._db)

    def get_tags_for_entry(self, entry):
        return self.get_queryset().for_entry(entry).order().select_related()


class EntryTagManager(models.Manager):

    def get_queryset(self):
        return querysets.EntryTagQuerySet(self.model, using=self._db)

    def get_published_tags(self):
        queryset = (self.get_queryset()
                    .published()
                    .order()
                    .select_related())
        return [entry_tag.tag for entry_tag in queryset]

    def get_entries_by_tag_slug(self, tag_slug):
        queryset = (self.get_queryset()
                    .published()
                    .with_slug(tag_slug)
                    .order()
                    .select_related())
        return [entry_tag.entry for entry_tag in queryset]