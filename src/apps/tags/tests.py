from django.test import TestCase
from django.core.urlresolvers import reverse

from entries.factories import EntryTestFactory

from . import factories


class TagListViewTests(TestCase):

    @classmethod
    def setUpClass(cls):
        cls.tag_factory = factories.TagTestFactory()
        cls.entry_factory = EntryTestFactory()
        cls.url = reverse('tags#tag_list')

    def test_status_200(self):
        response = self.client.get(self.url)
        self.assertEqual(200, response.status_code)

    def test_not_contains_not_published_tag_name(self):
        tag = self.tag_factory.create()
        response = self.client.get(self.url)
        self.assertNotContains(response, tag.name)

    def test_response_contains_published_tag_name(self):
        tag = self.tag_factory.create()
        self.entry_factory.create_published(tags=[tag])
        response = self.client.get(self.url)
        self.assertContains(response, tag.name)


class TaggedEntryListView(TestCase):

    @classmethod
    def setUpClass(cls):
        cls.tag_factory = factories.TagTestFactory()
        cls.entry_factory = EntryTestFactory()

    def test_status_200(self):
        tag = self.tag_factory.create()
        url = reverse('tags#tagged_entry_list', args=[tag.slug])
        response = self.client.get(url)
        self.assertEqual(200, response.status_code)

    def test_contains_entries_for_tag(self):
        tag = self.tag_factory.create()
        entries = [
            self.entry_factory.create_published(tags=[tag]),
            self.entry_factory.create_published(tags=[tag])
        ]
        url = reverse('tags#tagged_entry_list', args=[tag.slug])
        response = self.client.get(url)
        self.assertContains(response, entries[0].subject)
        self.assertContains(response, entries[1].subject)

    def test_doesnt_contain_entry_for_diff_tag(self):
        tag = self.tag_factory.create()
        other_tag = self.tag_factory.create()
        entry = self.entry_factory.create_published(tags=[tag])
        url = reverse('tags#tagged_entry_list', args=[other_tag.slug])
        response = self.client.get(url)
        self.assertNotContains(response, entry.subject)
