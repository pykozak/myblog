from commons.factories import create_text_gen

from tags.models import EntryTag

from . import models


class EntryTestFactory:
    subject_gen = create_text_gen('subject_{0}')
    template_gen = create_text_gen('template_{0}')
    default_status = models.Entry.STATUS_DRAFT

    def create(self, subject=None, template=None, tags=()):
        return self.create_draft(subject, template, tags)

    def create_draft(self, subject=None, template=None, tags=()):
        entry = self._build(subject, template, models.Entry.STATUS_DRAFT)
        entry.save()
        self._create_entry_tags(entry, tags)
        return entry

    def create_published(self, subject=None, template=None, tags=()):
        entry = self._build(subject, template, models.Entry.STATUS_PUBLISHED)
        entry.save()
        self._create_entry_tags(entry, tags)
        return entry

    def create_deleted(self, subject=None, template=None, tags=()):
        entry = self._build(subject, template, models.Entry.STATUS_DELETED)
        entry.save()
        self._create_entry_tags(entry, tags)
        return entry

    def _build(self, subject=None, template=None, status=None):
        if subject is None:
            subject = next(self.subject_gen)

        if template is None:
            template = next(self.template_gen)

        return models.Entry(
            subject=subject,
            template=template,
            status=status or self.default_status
        )

    def _create_entry_tags(self, entry, tags=()):
        entry_tags = [EntryTag(entry=entry, tag=tag) for tag in tags]
        EntryTag.objects.bulk_create(entry_tags)