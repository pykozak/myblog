from django.db import models
from django.db.models.loading import get_model
from django.template.loader import Template, Context
from django.utils.timezone import now

import markdown

from commons.models import BaseModel

from . import managers


class Entry(BaseModel):
    STATUS_DRAFT = 10
    STATUS_PUBLISHED = 20
    STATUS_DELETED = 100
    STATUS_CHOICES = (
        (STATUS_DRAFT, 'Szkic'),
        (STATUS_PUBLISHED, 'Opublikowany'),
        (STATUS_DELETED, 'Usunięty')
    )
    published_at = models.DateTimeField(
        verbose_name='Data pierwszej publikacji',
        blank=True,
        null=True
    )
    slug = models.SlugField(
        verbose_name='Slug',
        max_length=255
    )
    subject = models.CharField(
        verbose_name='Temat',
        max_length=255
    )
    template = models.TextField(
        verbose_name='Szablon',
        blank=True
    )
    content = models.TextField(
        verbose_name='Treść',
        blank=True
    )
    status = models.IntegerField(
        verbose_name='Status',
        choices=STATUS_CHOICES,
        default=STATUS_PUBLISHED
    )
    objects = managers.EntryManager()

    class Meta:
        verbose_name = 'Wpis'
        verbose_name_plural = 'Wpisy'

    def __str__(self):
        return '<Entry: {0}>'.format(self.subject)
    
    def save(self, *args, **kwargs):
        self.slug = self.subject

        if not self.published_at:
            if self.status == self.STATUS_PUBLISHED:
                self.published_at = now()

        return super(Entry, self).save(*args, **kwargs)

    def render(self):
        context = Context({s.name: s.render() for s in self.snippet_set.all()})
        context.update({i.name: i.render() for i in self.image_set.all()})
        html_template = markdown.markdown(self.template)
        return Template(html_template).render(context)

    def get_tags(self):
        model = get_model('tags', 'Tag')
        return model.objects.get_tags_for_entry(self)

    def get_comments(self):
        return self.commententry_set.order_by('created_at')