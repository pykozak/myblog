from django.views import generic
from django.core.urlresolvers import reverse_lazy
from django.db import transaction
from django.shortcuts import render, get_object_or_404, redirect

from comments.forms import CommentEntryForm

from . import models


class EntryListView(generic.ListView):
    queryset = models.Entry.objects.published().order_by('-published_at')
    context_object_name = 'entries'
    paginate_by = 5


class EntryDetailView(generic.View):
    template_name = 'entries/entry_detail.html'
    queryset = models.Entry.objects.published()
    form_class = CommentEntryForm
    success_url = reverse_lazy('entries#entry_list')

    def get(self, request, entry_id):
        entry = get_object_or_404(self.queryset, id=entry_id)
        form = self.form_class(request, initial={'entry': entry})
        context = {'entry': entry, 'form': form}
        return render(request, self.template_name, context)