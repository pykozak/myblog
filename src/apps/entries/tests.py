from django.test import TestCase
from django.core.urlresolvers import reverse

from users.clients import AdminClient, UserClient
from comments.factories import CommentEntryTestFactory

from . import factories


class PreviewEntryDetailViewTests(TestCase):

    @classmethod
    def setUpClass(cls):
        cls.entry_factory = factories.EntryTestFactory()

    def setUp(self):
        self.client = AdminClient()
        self.client.login()

    def test_status_200_for_published_entry(self):
        entry = self.entry_factory.create_published()
        url = reverse('admin:entries#preview_entry_detail', args=[entry.id])
        response = self.client.get(url)
        self.assertEqual(200, response.status_code)

    def test_status_200_for_draft_entries(self):
        entry = self.entry_factory.create_draft()
        url = reverse('admin:entries#preview_entry_detail', args=[entry.id])
        response = self.client.get(url)
        self.assertEqual(200, response.status_code)

    def test_status_200_for_deleted_entries(self):
        entry = self.entry_factory.create_deleted()
        url = reverse('admin:entries#preview_entry_detail', args=[entry.id])
        response = self.client.get(url)
        self.assertEqual(200, response.status_code)

    def test_contains_subject(self):
        entry = self.entry_factory.create_published()
        url = reverse('admin:entries#preview_entry_detail', args=[entry.id])
        response = self.client.get(url)
        self.assertContains(response, entry.subject)


class EntryDetailViewTests(TestCase):

    @classmethod
    def setUpClass(cls):
        cls.entry_factory = factories.EntryTestFactory()
        cls.comment_factory = CommentEntryTestFactory()

    def get_comment_form_valid_data(self, entry, **kwargs):
        defaults = {
            'entry': entry.id,
            'username': 'username_1',
            'email': 'example@mail.local',
            'template': 'Hi!'
        }
        defaults.update(kwargs)
        return defaults

    def test_status_200_for_published_entry(self):
        entry = self.entry_factory.create_published()
        url = reverse('entries#entry_detail', args=[entry.id])
        response = self.client.get(url)
        self.assertEqual(200, response.status_code)

    def test_status_404_for_draft_entries(self):
        entry = self.entry_factory.create_draft()
        url = reverse('entries#entry_detail', args=[entry.id])
        response = self.client.get(url)
        self.assertEqual(404, response.status_code)

    def test_status_404_for_deleted_entries(self):
        entry = self.entry_factory.create_deleted()
        url = reverse('entries#entry_detail', args=[entry.id])
        response = self.client.get(url)
        self.assertEqual(404, response.status_code)

    def test_contains_subject(self):
        entry = self.entry_factory.create_published()
        url = reverse('entries#entry_detail', args=[entry.id])
        response = self.client.get(url)
        self.assertContains(response, entry.subject)


class EntryListViewTests(TestCase):

    @classmethod
    def setUpClass(cls):
        cls.entry_factory = factories.EntryTestFactory()

    def test_status_200(self):
        response = self.client.get(reverse('entries#entry_list'))
        self.assertEqual(200, response.status_code)

    def test_draft_entries_arent_displayed(self):
        entry = self.entry_factory.create_draft()
        response = self.client.get(reverse('entries#entry_list'))
        self.assertNotContains(response, entry.subject)

    def test_published_entries_are_displayed(self):
        entry = self.entry_factory.create_published()
        response = self.client.get(reverse('entries#entry_list'))
        self.assertContains(response, entry.subject)

    def test_deleted_entries_arent_displayed(self):
        entry = self.entry_factory.create_deleted()
        response = self.client.get(reverse('entries#entry_list'))
        self.assertNotContains(response, entry.subject)
