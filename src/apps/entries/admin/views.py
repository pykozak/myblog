from django.views import generic
from django.shortcuts import render, get_object_or_404

from .. import models


class PreviewEntryDetailView(generic.View):

    def get(self, request, entry_id):
        entry = get_object_or_404(models.Entry.objects.all(), id=entry_id)
        context = {'entry': entry}
        return render(request, 'entries/preview_entry_detail.html', context)
