from django.conf.urls import patterns, url

from . import views

urlpatterns = patterns('',
    url(
        regex=r'^podglad/(?P<entry_id>\d+)/$',
        view=views.PreviewEntryDetailView.as_view(),
        name='entries#preview_entry_detail'
    )
)
