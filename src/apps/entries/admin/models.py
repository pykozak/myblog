from django.contrib import admin

from commons.admin import BaseModelAdmin
from comments.admin import CommentEntryInline
from snippets.admin import SnippetInline
from images.admin import ImageInline
from tags.admin import EntryTagInline

from .. import models

from . import urls


class EntryAdmin(BaseModelAdmin):
    urlpatterns = urls.urlpatterns
    permissions = ['add', 'change']
    list_display = ['subject', 'created_at', 'published_at', 'status']
    readonly_fields = ['published_at', 'slug', 'content']
    fieldsets = (
        ('Pola do tworzenia/edycji', {
            'fields': ('subject', 'template', 'status')
        }),
        ('Dodatkowe informacje', {
            'classes': ('collapse',),
            'fields': ('published_at', 'content', 'slug')
        })
    )
    inlines = [SnippetInline, ImageInline, EntryTagInline, CommentEntryInline]

    def get_queryset(self, request):
        queryset = super(EntryAdmin, self).get_queryset(request)
        return queryset.order_by('-created_at')

admin.site.register(models.Entry, EntryAdmin)
