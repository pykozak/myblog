from django.db.models.query import QuerySet


class EntryQuerySet(QuerySet):

    def published(self):
        return self.filter(status=self.model.STATUS_PUBLISHED)
