from django.conf.urls import patterns, url

from . import views

urlpatterns = patterns('',
    url(
        regex=r'^$',
        view=views.EntryListView.as_view(),
        name='entries#entry_list'
    ),
    url(
        regex=r'^(?P<page>\d+)/$',
        view=views.EntryListView.as_view(),
        name='entries#entry_list_page'
    ),
    url(
        regex=r'^wpis/(?P<entry_id>\d+)/$',
        view=views.EntryDetailView.as_view(),
        name='entries#entry_detail'
    )
)
