from django.db import models

from . import querysets


class EntryManager(models.Manager):
    title = 'Wiadomości z MOJASTRONA.pl'

    def get_queryset(self):
        return querysets.EntryQuerySet(self.model, using=self._db)

    def published(self):
        return self.get_queryset().published()
    
    def get_latest_published_entries(self, max_count):
        return (self.get_queryset().published()
                .order_by('-published_at')[:max_count])
