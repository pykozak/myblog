from commons.factories import create_text_gen

from . import models


class UserTestFactory:
    username_gen = create_text_gen('username_{0}')
    email_gen = create_text_gen('username{0}@email.local')
    default_password = '123456'

    def create(self, username=None, email=None, password=None):
        return self._create(
            username=username or next(self.username_gen),
            email=email or next(self.email_gen),
            password=password or self.default_password
        )

    def _create(self, **kwargs):
        return models.User.objects.create_user(**kwargs)


class AdminTestFactory(UserTestFactory):

    def _create(self, **kwargs):
        return models.User.objects.create_superuser(**kwargs)
