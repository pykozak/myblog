from django.test import Client

from . import factories


class UserClient(Client):
    user_factory = factories.UserTestFactory()

    def __init__(self,  user=None, **kwargs):
        self.user = user or self.user_factory.create()
        super(UserClient, self).__init__(**kwargs)

    def login(self, password=None):
        return super(UserClient, self).login(
            username=self.user.username,
            password=password or self.user_factory.default_password
        )


class AdminClient(UserClient):
    user_factory = factories.AdminTestFactory()
