from django.contrib import admin

from commons.admin import BaseModelAdmin

from . import models


class ProgrammingLanguageAdmin(BaseModelAdmin):
    permissions = ['add', 'change']
    list_display = ['name', 'file_extension', 'created_at']


admin.site.register(models.ProgrammingLanguage, ProgrammingLanguageAdmin)


class SnippetInline(admin.StackedInline):
    model = models.Snippet
    extra = 1
    fields = ['source', 'name', 'language']
