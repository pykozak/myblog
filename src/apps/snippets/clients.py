import json
import requests


class GistClient:
    API_URL = 'https://api.github.com/gists'

    def __init__(self, http_provider=requests):
        self.http_provider = http_provider

    def post_file(self, filename, content):
        data = {
            'public': True,
            'files': {
                filename: {'content': content}
            }
        }
        response = self.http_provider.post(self.API_URL, data=json.dumps(data))
        return response.json()['html_url']


class GistSnippetClient:

    def __init__(self, gist_client=None):
        self.gist_client = gist_client or GistClient()

    def post_snippet(self, snippet):
        return self.gist_client.post_file(snippet.filename, snippet.source)