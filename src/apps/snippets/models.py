from django.db import models

from commons.models import BaseModel

from . import clients


class ProgrammingLanguage(BaseModel):
    name = models.CharField(
        verbose_name='Nazwa',
        max_length=50
    )
    file_extension = models.CharField(
        verbose_name='Rozszerzenie pliku',
        max_length=5
    )

    class Meta:
        verbose_name = 'Język programowania'
        verbose_name_plural = 'Języki programowania'

    def __str__(self):
        return self.name


class Snippet(BaseModel):
    name = models.CharField(
        max_length=100,
        verbose_name='Nazwa'
    )
    entry = models.ForeignKey(
        'entries.Entry',
        verbose_name='Wpis'
    )
    language = models.ForeignKey(
        'ProgrammingLanguage',
        verbose_name='Język'
    )
    source = models.TextField(
        verbose_name='Kod'
    )
    external_url = models.URLField(
        verbose_name='URL na gist'
    )

    class Meta:
        verbose_name = 'Wstawka kodu'
        verbose_name_plural = 'Wstawki kodu'
        unique_together = ('name', 'entry')

    def __str__(self):
        return '<Snippet: {0} dla Entry: {1}>'.format(self.id, self.entry.id)

    def save(self, *args, **kwargs):
        client = clients.GistSnippetClient()
        self.external_url = client.post_snippet(self)
        return super(Snippet, self).save(*args, **kwargs)

    @property
    def filename(self):
        if not self.name:
            raise ValueError('Brak nazwy.')

        try:
            file_extension = self.language.file_extension
        except models.ObjectDoesNotExist:
            return self.name
        else:
            return '{0}.{1}'.format(self.name, file_extension)

    def render(self):
        return '<script src="{0}.js"></script>'.format(self.external_url)
