from django.db import models
from django.utils.html import format_html

from commons.models import BaseModel


class Image(BaseModel):
    name = models.CharField(
        max_length=100,
        verbose_name='Nazwa'
    )
    entry = models.ForeignKey(
        'entries.Entry',
        verbose_name='Wpis'
    )
    external_url = models.URLField(
        verbose_name='URL na imgur'
    )

    class Meta:
        verbose_name = 'Obrazek'
        verbose_name_plural = 'Obrazki'

    def __str__(self):
        return self.name

    def render(self):
        if self.external_url:
            return format_html('<img src="{0}" />', self.external_url)
        return ''