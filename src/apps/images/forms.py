from django import forms
from django.conf import settings

from . import models
from . import clients


class ImageAdminForm(forms.ModelForm):
    image_data = forms.FileField(
        label='Wybierz plik'
    )

    class Meta:
        model = models.Image
        fields = ('name', 'image_data')

    def clean(self):
        try:
            self.cleaned_data['image_data']
        except KeyError:
            del self.errors['image_data']
        return self.cleaned_data

    def save(self, *args, **kwargs):
        client = clients.ImgurClient(
            client_id=settings.IMGUR_CLIENT_ID,
            api_key=settings.IMGUR_API_KEY
        )
        image_data = next(self.cleaned_data['image_data'].chunks())
        self.instance.external_url = client.post_image_data(image_data)
        return super(ImageAdminForm, self).save(*args, **kwargs)