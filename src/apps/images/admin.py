from django.contrib import admin

from . import models
from . import forms


class ImageInline(admin.StackedInline):
    model = models.Image
    form = forms.ImageAdminForm
    fields = ('name', 'preview', 'image_data', 'external_url')
    readonly_fields = ('external_url', 'preview')
    extra = 1

    def preview(self, image):
        return image.render()

    preview.short_description = 'Podgląd'

    def get_fieldsets(self, request, image=None):
        if image:
            return (
                (None, {
                    'fields': ('name', 'preview', 'image_data')
                }),
                ('Szczegóły', {
                    'classes': ('collapse',),
                    'fields': ('external_url',)
                })
            )
        else:
            return (
                (None, {
                    'fields': ('name', 'image_data')
                }),
            )
