import base64
import json

import requests


class ImgurClient:
    API_URL = 'https://api.imgur.com/3/upload.json'

    def __init__(self, client_id, api_key, http_provider=requests):
        self.client_id = client_id
        self.headers = {'Authorization': 'Client-ID {0}'.format(self.client_id)}
        self.api_key = api_key
        self.http_provider = http_provider

    def post_image_data(self, image_data):
        response = self.http_provider.post(
            self.API_URL, headers=self.headers, data={
                'key': self.api_key,
                'image': base64.b64encode(image_data),
                'type': 'base64'
            }
        )
        if response.status_code == 200:
            results = json.loads(response.text)
            return results['data']['link']